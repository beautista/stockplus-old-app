/**
 * 
 */

var object = $.extend({},UtilPagination);

function isZero(e){
	var check = false;
	if(!$(e).val() || $(e).val() == 0|| $(e).val() == '0'||$(e).val() == 0.0|| $(e).val() == '0.0'){
		check = true;
	}
	return check;
}

function setZeroWhenNull(e){
	if(isNaN(e) || !isFinite(e)){
		return 0;
	}else{
		return e;
	}
	
}

function numberWithCommas(x) {
	if(x){
		x = parseFloat(x).toFixed(2)
	}else{
		x = 0.0;
	}
	
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}



function searchReport(){
	var criteriaObject = {
			year    : $("#search_year").val()==""?"null":$("#search_year").val()
							
	    };
	queryReportByCriteria(criteriaObject);
}

function queryReportByCriteria(criteriaObject){
	object.setId("#paggingReport");                
    object.setUrlData("/report/summary/findByCriteria"); 
    object.setUrlSize("/report/summary/findSize"); 
    
    object.setLimitData(17);
    
    object.loadTable = function(item){
        $('.dv-background').show();
        $('#gridReportBody').empty();
        $dataForQuery = item;
        for(var j=0;j<item.length;j++){
            var monthName = item[j].monthName==null?"":item[j].monthName;
            var subTotalAmount = item[j].subTotalAmount==null?"":item[j].subTotalAmount;
            var rebateAmount = item[j].rebateAmount==null?"":item[j].rebateAmount;
            var vatAmount = item[j].vatAmount==null?"":item[j].vatAmount;
            var totalAmount = item[j].totalAmount==null?"":item[j].totalAmount;
            var subTotalAfterRebate = item[j].subTotalAfterRebate==null?"":item[j].subTotalAfterRebate;
            var itemCostAmount = item[j].itemCostAmount==null?"":item[j].itemCostAmount;
            var contritutionAmount = item[j].contritutionAmount==null?"":item[j].contritutionAmount;
            var marginAmount = item[j].marginAmount==null?"":item[j].marginAmount;
            
            var classTr = "trlightgray";
            if(monthName.includes("Quarter")){
            	classTr = "trquarter";
            }else if(monthName.includes("Summary")){
            	classTr = "trsummary";
            }else if((j%2 == 0)){
            	classTr = "trwhite";
            }
        
            
            
            $('#gridReportBody').append(''+
                    '<tr id="'+item[j].id+'" '+' class="'+classTr+'"'+'>'+
                    '<td class="text-center" >'+monthName+'</td>'+
                    '<td class="text-center" >'+numberWithCommas(subTotalAmount)+'</td>'+
                    '<td class="text-center" >'+numberWithCommas(rebateAmount)+'</td>'+
                    '<td class="text-center" >'+numberWithCommas(vatAmount)+'</td>'+
                    '<td class="text-center" >'+numberWithCommas(totalAmount)+'</td>'+
                    '<td class="text-center" >'+numberWithCommas(subTotalAfterRebate)+'</td>'+
                    '<td class="text-center" >'+numberWithCommas(itemCostAmount)+'</td>'+
                    '<td class="text-center" >'+numberWithCommas(contritutionAmount)+'</td>'+
                    '<td class="text-center" >'+numberWithCommas(marginAmount)+'</td>'+
                    '</tr>'
                );
        }
        $('.dv-background').hide();
    };

    object.setDataSearch(criteriaObject); 

    object.search(object); 
}









$(document).ready(function () {
	
	$('.numbersOnly').on("keydown",function(e){
        NumberUtil.numberOnly(e);
    });
	
	$('.amount2digit').on("keydown",function(e){
        NumberUtil.numberOnly(e);
        NumberUtil.numberFixDigit($(this),2);
    });
	
	$('#search_button').on('click', function () {
		searchReport();
	});
	
	
	
	
	searchReport();
	
});

