package com.spt.app.spring.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.ldap.authentication.ad.ActiveDirectoryLdapAuthenticationProvider;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Autowired
    @Qualifier("customUserDetailsService")
    UserDetailsService userDetailsService;
	
	@Autowired
    public void configureGlobalSecurity(AuthenticationManagerBuilder auth) throws Exception {
		//1.Default Memory Authentication Provider
		auth.inMemoryAuthentication().withUser("system").password("manager").roles("SUPER_ADMIN");
				
		//Set Sequence of Provider
		//2.Custom Authentication Provider
		auth.userDetailsService(userDetailsService).passwordEncoder((PasswordEncoder) userDetailsService);

		
	}
	
	@Override
    protected void configure(HttpSecurity http) throws Exception {
    	http.authorizeRequests()
        .antMatchers("/login/**", 
        		     "/barcodescan/**",
        		     "/resources/images/**", 
        		     "/resources/fonts/**",
        		     "/resources/logs/**",
        		     "/lbAccessStatus/**").permitAll() 
        .antMatchers("/resources/**").authenticated()
        .antMatchers("/**").access("hasAnyRole('USER','ADMIN','SUPER_ADMIN','USER_LEVEL1','USER_LEVEL2')")
        .and().logout().logoutUrl("/logout")
        .and().formLogin().loginPage("/login").loginProcessingUrl("/j_spring_security_check").usernameParameter("j_username").passwordParameter("j_password").successHandler(getAuthenticationSuccessHandler())
        .and().exceptionHandling().authenticationEntryPoint(getAuthenticationEntryPoint())
        						  .accessDeniedPage("/Access_Denied")
        						  ;
    	http.sessionManagement().maximumSessions(20).expiredUrl("/logout");
    }
	
	@Bean
	public CustomAuthenticationEntryPoint getAuthenticationEntryPoint() {
		CustomAuthenticationEntryPoint authenticationEntryPoint = new CustomAuthenticationEntryPoint();
		authenticationEntryPoint.setLoginPageUrl("/login");
		authenticationEntryPoint.setReturnParameterEnabled(true);
		authenticationEntryPoint.setReturnParameterName("r");
	
		return authenticationEntryPoint;
	}
	
	@Bean
	public CustomAuthenticationSuccessHandler getAuthenticationSuccessHandler() {
		CustomAuthenticationSuccessHandler authenticationHandler = new CustomAuthenticationSuccessHandler();
		authenticationHandler.setDefaultTargetUrl("/");
		authenticationHandler.setTargetUrlParameter("spring-security-redirect");
	
		return authenticationHandler;
	}
	
	
	
	
	
	
	
}
