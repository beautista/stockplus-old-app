package com.spt.app.spring.websocket.barcodescan;

public class OutputMessage {

	private String saler;
    private String barcode;
    private String time;


	public OutputMessage(String saler, String barcode, String time) {
		this.saler = saler;
		this.barcode = barcode;
		this.time = time;
	}

	public String getSaler() {
		return saler;
	}

	public void setSaler(String saler) {
		this.saler = saler;
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

    
}
