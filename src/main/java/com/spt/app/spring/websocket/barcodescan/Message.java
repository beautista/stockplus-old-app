package com.spt.app.spring.websocket.barcodescan;

public class Message {

	private String saler;
    private String barcode;
    
	public String getSaler() {
		return saler;
	}
	public void setSaler(String saler) {
		this.saler = saler;
	}
	public String getBarcode() {
		return barcode;
	}
	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}
    
    
    
}
