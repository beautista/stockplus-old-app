package com.spt.app.spring.websocket.barcodescan;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
public class BarcodeController {

	static final Logger LOGGER = LoggerFactory.getLogger(BarcodeController.class);
	
	
	@MessageMapping("/barcodescan")
    @SendTo("/topic/messages")
    public OutputMessage send(final Message message) throws Exception {

        final String time = new SimpleDateFormat("HH:mm").format(new Date());
        return new OutputMessage(message.getSaler(), message.getBarcode(), time);
    }
	
	
}
