package com.spt.app.spring.configuration;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.spt.app.util.SerializeUtil;

public class RememberFilterInteceptor extends HandlerInterceptorAdapter{

	static final Logger LOGGER = LoggerFactory.getLogger(RememberFilterInteceptor.class);
	
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response,  Object handler) throws Exception  {
		
		
		HandlerMethod hm = (HandlerMethod)handler; 
		try {
			String[] getMapping = hm.getMethod().getAnnotation(GetMapping.class).value();
			if(getMapping!=null && getMapping.length >= 1){
				String pageName = getMapping[0].replace("/findByCriteria", "").substring(1);
				Cookie cookie = new Cookie(pageName+"-search-filter",new  String(SerializeUtil.mapToStream(request.getParameterMap())));
				response.addCookie(cookie);
			}
		} catch (Exception e) { }
		
		try {
			String[] postMapping = hm.getMethod().getAnnotation(PostMapping.class).value();
			if(postMapping!=null && postMapping.length >= 1){
				String pageName = postMapping[0].replace("/findByCriteria", "").substring(1);
				Cookie cookie = new Cookie(pageName+"-search-filter",new  String(SerializeUtil.mapToStream(request.getParameterMap())));
				response.addCookie(cookie);
			}
		} catch (Exception e) { }
		
		
		return true;
	}
	
}
