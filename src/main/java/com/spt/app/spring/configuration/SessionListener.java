package com.spt.app.spring.configuration;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SessionListener implements HttpSessionListener {
	
	static final Logger LOGGER = LoggerFactory.getLogger(SessionListener.class);
 
    @Override
    public void sessionCreated(HttpSessionEvent event) {
    	LOGGER.debug("==== Session is created ====");
        event.getSession().setMaxInactiveInterval(60 * 60 * 24);
    }
 
    @Override
    public void sessionDestroyed(HttpSessionEvent event) {
    	LOGGER.debug("==== Session is destroyed ====");
    }
}
