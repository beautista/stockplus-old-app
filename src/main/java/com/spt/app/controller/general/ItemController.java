package com.spt.app.controller.general;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.spt.app.aop.annotation.Loggable;
import com.spt.app.service.ItemService;

@Controller
public class ItemController {
	static final Logger LOGGER = LoggerFactory.getLogger(ItemController.class);
	
	@Autowired
	ItemService itemService;

	@GetMapping("/items/listView")
	public String getItemList() {
		return "items";
	}
	
	@GetMapping(value="/items/findItemSize",produces="application/json")
	@ResponseBody
	public ResponseEntity<String>  findItemSize(HttpServletRequest request) {
		Map<String, Object> objectMap = new HashMap<String, Object>();
        objectMap.put("queryString", request.getQueryString());
		HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String>  responseEntity =  itemService.findItemSize(objectMap);
		return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
	}
	
	
	@GetMapping(value="/items/findItemByCriteria",produces="application/json", headers = "Accept=application/json")
	@ResponseBody
	public ResponseEntity<String>  findItemByCriteria(HttpServletRequest request) {
        Map<String, Object> objectMap = new HashMap<String, Object>();
        objectMap.put("queryString", request.getQueryString());
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String>  responseEntity =  itemService.findByCriteria(objectMap);
        return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        
	}
	
	@PostMapping(value = "/items/save", produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8") 
	public ResponseEntity createItem(HttpServletRequest request) {
		Map<String,String[]> mapClone = new HashMap<String,String[]>(request.getParameterMap());
		itemService.save(mapClone);
		return new ResponseEntity<String>("OK", HttpStatus.OK);
	}
	
	@DeleteMapping(value = "/items/{id}")
	public ResponseEntity deleteItem(@PathVariable Long id) {
		itemService.dalete(id);
		return new ResponseEntity("", HttpStatus.OK);
	}
	
	@GetMapping("/items/{id}")
	public ResponseEntity getItem(@PathVariable Long id) {
		HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String>  responseEntity =  itemService.getItem(id);
        return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
	}
	
}
