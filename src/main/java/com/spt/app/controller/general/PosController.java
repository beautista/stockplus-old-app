package com.spt.app.controller.general;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spt.app.service.PosService;
import com.spt.app.spring.logback.LogMessage;
import com.spt.app.spring.logback.LogOutputMessage;
import com.spt.app.spring.websocket.chat.Message;

@Controller
public class PosController {
	static final Logger LOGGER = LoggerFactory.getLogger(PosController.class);
	
	@Autowired
	PosService posService;
	
	@Autowired
	private SimpMessagingTemplate messageSender;
	
	/*@MessageMapping("/log")
    @SendTo("/topic/barcodescan/messages")
    public Message send(final Message message) throws Exception {
        return message;
    }*/

	@GetMapping("/barcodescan/{saler}/{barcode}")
	@ResponseBody
	public ResponseEntity<String> barcodescan(@PathVariable String saler,@PathVariable String barcode) {
		HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        Message message = new Message();
        message.setFrom(saler);
        message.setText(barcode);
        messageSender.convertAndSend("/topic/barcodescan/messages", message);
		return new ResponseEntity<String>("OK", headers, HttpStatus.OK);
	}
	

	@GetMapping("/pos/listView")
	public String getPosList() {
		return "pos";
	}
	
	@GetMapping(value="/pos/findSize",produces="application/json")
	@ResponseBody
	public ResponseEntity<String>  findSize(HttpServletRequest request) {
		Map<String, Object> objectMap = new HashMap<String, Object>();
        objectMap.put("queryString", request.getQueryString());
		HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String>  responseEntity =  posService.findSize(objectMap);
		return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
	}
	
	
	@GetMapping(value="/pos/findByCriteria",produces="application/json", headers = "Accept=application/json")
	@ResponseBody
	public ResponseEntity<String>  findByCriteria(HttpServletRequest request) {
        Map<String, Object> objectMap = new HashMap<String, Object>();
        objectMap.put("queryString", request.getQueryString());
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String>  responseEntity =  posService.findByCriteria(objectMap);
        return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        
	}
	
	@DeleteMapping(value = "/pos/{saler}")
	public ResponseEntity deleteItem(@PathVariable String saler) {
		posService.deleteSalesTransactionByUser(saler);
		return new ResponseEntity("", HttpStatus.OK);
	}
	

	@DeleteMapping(value = "/pos/delete/{id}")
	public ResponseEntity deleteItem(@PathVariable Long id) {
		posService.deleteSalesTransaction(id);
		return new ResponseEntity("", HttpStatus.OK);
	}
	
	@PostMapping(value = "/pos/save", produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8") 
	public ResponseEntity createItem(HttpServletRequest request) {
		Map<String,String[]> mapClone = new HashMap<String,String[]>(request.getParameterMap());
		posService.saveSalesTransaction(mapClone);
		return new ResponseEntity<String>("OK", HttpStatus.OK);
	}
	

	
	@PostMapping(value = "/pos/so/save", produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8") 
	public ResponseEntity saveSo(HttpServletRequest request) {
		Map<String,String[]> mapClone = new HashMap<String,String[]>(request.getParameterMap());
		ResponseEntity responseEntity = posService.saveSo(mapClone);
		return new ResponseEntity<String>((String) responseEntity.getBody(), HttpStatus.OK);
	}
	
	@GetMapping(value="/pos/printSlip/{soNumber}",produces="application/json")
	@ResponseBody
	public ResponseEntity<String>  printSlip(@PathVariable String soNumber) {
		LOGGER.info("printSlip={}",soNumber);
		HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String>  responseEntity =  posService.printSlip(soNumber);
		return new ResponseEntity<String>("", headers, HttpStatus.OK);
	}
}
