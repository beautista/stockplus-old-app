package com.spt.app.controller.general;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spt.app.service.PosService;
import com.spt.app.service.ReportService;

@Controller
public class ReportController {
	static final Logger LOGGER = LoggerFactory.getLogger(ReportController.class);
	
	@Autowired
	ReportService reportService;
	
	@GetMapping("/report/item")
	public String reportItem() {
		return "reportitem";
	}
	
	@GetMapping("/report/sales")
	public String reportSales() {
		return "reportsales";
	}
	
	@GetMapping("/report/summary")
	public String reportSummary() {
		return "reportsummary";
	}
	
	@GetMapping(value="/report/item/findSize",produces="application/json")
	@ResponseBody
	public ResponseEntity<String>   findSizeReportItem(HttpServletRequest request) {
		Map<String, Object> objectMap = new HashMap<String, Object>();
        objectMap.put("queryString", request.getQueryString());
		HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String>  responseEntity =  reportService.findSizeReportItem(objectMap);
		return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
	}
	
	
	@GetMapping(value="/report/item/findByCriteria",produces="application/json", headers = "Accept=application/json")
	@ResponseBody
	public ResponseEntity<String>  findByCriteriaReportItem(HttpServletRequest request) {
        Map<String, Object> objectMap = new HashMap<String, Object>();
        objectMap.put("queryString", request.getQueryString());
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String>  responseEntity =  reportService.findByCriteriaReportItem(objectMap);
        return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        
	}
	

	@GetMapping(value="/report/sale/findSize",produces="application/json")
	@ResponseBody
	public ResponseEntity<String>   findSizeReportSale(HttpServletRequest request) {
		Map<String, Object> objectMap = new HashMap<String, Object>();
        objectMap.put("queryString", request.getQueryString());
		HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String>  responseEntity =  reportService.findSizeReportSale(objectMap);
		return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
	}
	
	
	@GetMapping(value="/report/sale/findByCriteria",produces="application/json", headers = "Accept=application/json")
	@ResponseBody
	public ResponseEntity<String>  findByCriteriaReportSale(HttpServletRequest request) {
        Map<String, Object> objectMap = new HashMap<String, Object>();
        objectMap.put("queryString", request.getQueryString());
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String>  responseEntity =  reportService.findByCriteriaReportSale(objectMap);
        return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        
	}
	

	

	@GetMapping(value="/report/summary/findSize",produces="application/json")
	@ResponseBody
	public ResponseEntity<String>   findSizeReportSummary(HttpServletRequest request) {
		Map<String, Object> objectMap = new HashMap<String, Object>();
        objectMap.put("queryString", request.getQueryString());
		HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String>  responseEntity =  reportService.findSizeReportSummary(objectMap);
		return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
	}
	
	
	@GetMapping(value="/report/summary/findByCriteria",produces="application/json", headers = "Accept=application/json")
	@ResponseBody
	public ResponseEntity<String>  findByCriteriaReportSummary(HttpServletRequest request) {
        Map<String, Object> objectMap = new HashMap<String, Object>();
        objectMap.put("queryString", request.getQueryString());
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String>  responseEntity =  reportService.findByCriteriaReportSummary(objectMap);
        return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        
	}
	
}
