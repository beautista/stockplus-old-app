package com.spt.app.controller.general;

import java.net.HttpCookie;
import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import com.spt.app.aop.annotation.Loggable;

@Controller
public class UserController {

	static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);
	
	@GetMapping("/users/listView")
	public String getUsersList() {
		return "users";
	}
	

	@Loggable
	@GetMapping("/users/findByCriteria")
	public List demosearch() {
		return null;
	}
	
//
//	@GetMapping("/users/{id}")
//	public ResponseEntity getCustomer(@PathVariable("id") Long id) {
//
//		Customer customer = customerDAO.get(id);
//		if (customer == null) {
//			return new ResponseEntity("No Customer found for ID " + id, HttpStatus.NOT_FOUND);
//		}
//
//		return new ResponseEntity(customer, HttpStatus.OK);
//	}
//
//	@PostMapping(value = "/users")
//	public ResponseEntity createCustomer(@RequestBody Customer customer) {
//
//		customerDAO.create(customer);
//
//		return new ResponseEntity(customer, HttpStatus.OK);
//	}
//
//	@DeleteMapping("/users/{id}")
//	public ResponseEntity deleteCustomer(@PathVariable Long id) {
//
//		if (null == customerDAO.delete(id)) {
//			return new ResponseEntity("No Customer found for ID " + id, HttpStatus.NOT_FOUND);
//		}
//
//		return new ResponseEntity(id, HttpStatus.OK);
//
//	}
//
//	@PutMapping("/customers/{id}")
//	public ResponseEntity updateCustomer(@PathVariable Long id, @RequestBody Customer customer) {
//
//		customer = customerDAO.update(id, customer);
//
//		if (null == customer) {
//			return new ResponseEntity("No Customer found for ID " + id, HttpStatus.NOT_FOUND);
//		}
//
//		return new ResponseEntity(customer, HttpStatus.OK);
//	}
}
