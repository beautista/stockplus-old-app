package com.spt.app.controller.general;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.spt.app.aop.annotation.Loggable;
import com.spt.app.service.CustomerService;
import com.spt.app.service.ItemService;

@Controller
public class CustomerController {
	static final Logger LOGGER = LoggerFactory.getLogger(CustomerController.class);
	
	@Autowired
	CustomerService customerService;

	
	
	@GetMapping("/customer/id/{id}")
	public ResponseEntity getCustomerById(@PathVariable Long id) {
		HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String>  responseEntity =  customerService.getCustomer(id);
        return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
	}
	
	@GetMapping("/customer/code/{code}")
	public ResponseEntity getCustomerByCode(@PathVariable String code) {
		HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String>  responseEntity =  customerService.getCustomer(code);
        return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
	}
	
	@PostMapping(value = "/customer/save", produces = "text/html;charset=utf-8", headers = "Accept=application/json; charset=utf-8") 
	public ResponseEntity createItem(HttpServletRequest request) {
		Map<String,String[]> mapClone = new HashMap<String,String[]>(request.getParameterMap());
		customerService.save(mapClone);
		return new ResponseEntity<String>("OK", HttpStatus.OK);
	}
	
}
