package com.spt.app.controller.general;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spt.app.service.SaleOrderService;

@Controller
public class SaleOrderController {
	static final Logger LOGGER = LoggerFactory.getLogger(SaleOrderController.class);

	@Autowired
	SaleOrderService saleOrderService;
	
	@GetMapping("/saleorder/listView")
	public String getSaleOrderList() {
		return "saleorder";
	}
	
	@GetMapping(value="/saleorder/findSize",produces="application/json")
	@ResponseBody
	public ResponseEntity<String>  findSize(HttpServletRequest request) {
		Map<String, Object> objectMap = new HashMap<String, Object>();
        objectMap.put("queryString", request.getQueryString());
		HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String>  responseEntity =  saleOrderService.findSize(objectMap);
		return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
	}
	
	
	@GetMapping(value="/saleorder/findByCriteria",produces="application/json", headers = "Accept=application/json")
	@ResponseBody
	public ResponseEntity<String>  findByCriteria(HttpServletRequest request) {
        Map<String, Object> objectMap = new HashMap<String, Object>();
        objectMap.put("queryString", request.getQueryString());
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String>  responseEntity =  saleOrderService.findByCriteria(objectMap);
        return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        
	}
	
	@GetMapping(value="/saleorderitem/findSize",produces="application/json")
	@ResponseBody
	public ResponseEntity<String>  findSaleOrderItemSize(HttpServletRequest request) {
		Map<String, Object> objectMap = new HashMap<String, Object>();
        objectMap.put("queryString", request.getQueryString());
		HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String>  responseEntity =  saleOrderService.findSaleOrderItemSize(objectMap);
		return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
	}
	
	
	@GetMapping(value="/saleorderitem/findByCriteria",produces="application/json", headers = "Accept=application/json")
	@ResponseBody
	public ResponseEntity<String>  findSaleOrderItemByCriteria(HttpServletRequest request) {
        Map<String, Object> objectMap = new HashMap<String, Object>();
        objectMap.put("queryString", request.getQueryString());
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String>  responseEntity =  saleOrderService.findSaleOrderItemByCriteria(objectMap);
        return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        
	}
	
	@GetMapping(value="/saleorderitemcustomer/findSize",produces="application/json")
	@ResponseBody
	public ResponseEntity<String>  findSaleOrderItemCustomerSize(HttpServletRequest request) {
		Map<String, Object> objectMap = new HashMap<String, Object>();
        objectMap.put("queryString", request.getQueryString());
		HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String>  responseEntity =  saleOrderService.findSaleOrderItemCustomerSize(objectMap);
		return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
	}
	
	
	@GetMapping(value="/saleorderitemcustomer/findByCriteria",produces="application/json", headers = "Accept=application/json")
	@ResponseBody
	public ResponseEntity<String>  findSaleOrderItemCustomerByCriteria(HttpServletRequest request) {
        Map<String, Object> objectMap = new HashMap<String, Object>();
        objectMap.put("queryString", request.getQueryString());
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        ResponseEntity<String>  responseEntity =  saleOrderService.findSaleOrderItemCustomerByCriteria(objectMap);
        return new ResponseEntity<String>(responseEntity.getBody(), headers, HttpStatus.OK);
        
	}
}
