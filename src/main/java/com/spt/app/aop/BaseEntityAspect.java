package com.spt.app.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;

import com.spt.app.spring.security.CustomUserModel;

import java.security.Principal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Map;

@Component
@Aspect
public class BaseEntityAspect {
	static final Logger LOGGER = LoggerFactory.getLogger(BaseEntityAspect.class);

	@Autowired
	CustomUserModel customUserModel;
	

    private ThreadLocal<SimpleDateFormat> sdf = new ThreadLocal<SimpleDateFormat>() {
        @Override
        protected SimpleDateFormat initialValue() {
            return new SimpleDateFormat("[yyyy-mm-dd hh:mm:ss:SSS]");
        }
    };


    @Pointcut("execution(* com.spt.app.service.*.save*(..))")
    public void stampUserMethods() {
    }


    @Before("stampUserMethods()")
    public void stampUserMethods(JoinPoint jp) {
    	User user = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String methodName = jp.getSignature().getName();
        for(Object arg:jp.getArgs()){
        	if(arg instanceof Map ){
        		Map<String,Object> map = (Map<String,Object>)arg;
        		if(map.get("id")!=null && ((String[]) map.get("id"))[0].trim().length() > 0){
        			map.put("updateBy", new String[] { user.getUsername() } );
        			map.put("updateDate", new String[] { String.valueOf(System.currentTimeMillis()) });
        		}else{
        			map.put("createdBy", new String[] { user.getUsername() });
        			map.put("createdDate", new String[] { String.valueOf(System.currentTimeMillis()) });
        		}
        	}
        }
    }

}
