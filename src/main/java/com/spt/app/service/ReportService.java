package com.spt.app.service;

import java.util.List;
import java.util.Map;

import org.springframework.http.ResponseEntity;

public interface ReportService {
	
	public ResponseEntity<String> findByCriteriaReportItem(Map<String,Object> objectMap);
	public ResponseEntity<String> findSizeReportItem(Map<String,Object> objectMap);
	public ResponseEntity<String> findByCriteriaReportSale(Map<String,Object> objectMap);
	public ResponseEntity<String> findSizeReportSale(Map<String,Object> objectMap);
	public ResponseEntity<String> findByCriteriaReportSummary(Map<String,Object> objectMap);
	public ResponseEntity<String> findSizeReportSummary(Map<String,Object> objectMap);
}
