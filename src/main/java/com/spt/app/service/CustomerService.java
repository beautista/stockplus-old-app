package com.spt.app.service;

import java.util.List;
import java.util.Map;

import org.springframework.http.ResponseEntity;

public interface CustomerService {
	public ResponseEntity<String> getCustomer(Long id);
	public ResponseEntity<String> getCustomer(String code);
	public ResponseEntity<String> save(Map parameter);
}
