package com.spt.app.service.impl;

import java.util.Arrays;
import java.util.Map;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.spt.app.service.AbstractEngineService;
import com.spt.app.service.SaleOrderService;

@Service
public class SaleOrderServiceImpl extends AbstractEngineService implements SaleOrderService {

	@Override
	public ResponseEntity<String> findByCriteria(Map<String, Object> objectMap) {
		// TODO Auto-generated method stub
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        String queryString = "";
        if(objectMap.get("queryString") != null){
        	queryString = String.valueOf(objectMap.get("queryString"));
        }
        
        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String url = "/saleorder/findByCriteria?"+queryString;
        return getResultString(url,entity);
	}

	@Override
	public ResponseEntity<String> findSize(Map<String, Object> objectMap) {
		// TODO Auto-generated method stub
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        String queryString = "";
        if(objectMap.get("queryString") != null){
        	queryString = String.valueOf(objectMap.get("queryString"));
        }
        
        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String url = "/saleorder/findSize?"+queryString;
        return getResultString(url,entity);
	}

	@Override
	public ResponseEntity<String> findSaleOrderItemByCriteria(Map<String, Object> objectMap) {
		// TODO Auto-generated method stub
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        String queryString = "";
        if(objectMap.get("queryString") != null){
        	queryString = String.valueOf(objectMap.get("queryString"));
        }
        
        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String url = "/saleorderitem/findByCriteria?"+queryString;
        return getResultString(url,entity);
	}

	@Override
	public ResponseEntity<String> findSaleOrderItemSize(Map<String, Object> objectMap) {
		// TODO Auto-generated method stub
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        String queryString = "";
        if(objectMap.get("queryString") != null){
        	queryString = String.valueOf(objectMap.get("queryString"));
        }
        
        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String url = "/saleorderitem/findSize?"+queryString;
        return getResultString(url,entity);
	}
	
	@Override
	public ResponseEntity<String> findSaleOrderItemCustomerByCriteria(Map<String, Object> objectMap) {
		// TODO Auto-generated method stub
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        String queryString = "";
        if(objectMap.get("queryString") != null){
        	queryString = String.valueOf(objectMap.get("queryString"));
        }
        
        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String url = "/saleorderitemcustomer/findByCriteria?"+queryString;
        return getResultString(url,entity);
	}

	@Override
	public ResponseEntity<String> findSaleOrderItemCustomerSize(Map<String, Object> objectMap) {
		// TODO Auto-generated method stub
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        String queryString = "";
        if(objectMap.get("queryString") != null){
        	queryString = String.valueOf(objectMap.get("queryString"));
        }
        
        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String url = "/saleorderitemcustomer/findSize?"+queryString;
        return getResultString(url,entity);
	}

}
