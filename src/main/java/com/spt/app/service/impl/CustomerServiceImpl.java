package com.spt.app.service.impl;

import java.util.Map;

import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.spt.app.service.AbstractEngineService;
import com.spt.app.service.CustomerService;

@Service
public class CustomerServiceImpl  extends AbstractEngineService implements CustomerService {

	@Override
	public ResponseEntity<String> getCustomer(Long id) {
		// TODO Auto-generated method stub
		String url = "/customer/id/"+id;
		return getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url);
	}

	@Override
	public ResponseEntity<String> getCustomer(String code) {
		// TODO Auto-generated method stub
		String url = "/customer/code/"+code;
		return getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url);
	}

	@Override
	public ResponseEntity<String> save(Map parameter) {
		// TODO Auto-generated method stub
		String url = "/customer/save";
		return postWithMapParameter(parameter, HttpMethod.POST, url);
	}

}
