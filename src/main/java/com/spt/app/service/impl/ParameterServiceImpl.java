package com.spt.app.service.impl;

import java.util.Arrays;
import java.util.Base64;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.deser.std.JsonNodeDeserializer;
import com.spt.app.service.AbstractEngineService;
import com.spt.app.service.ParameterService;
import com.spt.app.service.UserAuthorizationService;
import com.spt.app.spring.security.CustomUser;
import com.spt.app.spring.security.CustomUserDetailsService;

@Service
public class ParameterServiceImpl extends AbstractEngineService implements ParameterService{

	static final Logger LOGGER = LoggerFactory.getLogger(ParameterServiceImpl.class);

	public Object getAllParameter() {
		// TODO Auto-generated method stub
		//Map map = gson.fromJson(getResultString(EngineServer+"parameters"), Map.class);
		return getResultString(EngineServer+"/parameters");
	}
	

}
