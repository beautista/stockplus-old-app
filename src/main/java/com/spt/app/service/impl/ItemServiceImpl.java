package com.spt.app.service.impl;

import java.util.Arrays;
import java.util.Map;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.spt.app.service.AbstractEngineService;
import com.spt.app.service.ItemService;

@Service
public class ItemServiceImpl extends AbstractEngineService implements ItemService {

	@Override
	public ResponseEntity<String> findByCriteria(Map<String, Object> objectMap) {
		// TODO Auto-generated method stub
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        String queryString = "";
        if(objectMap.get("queryString") != null){
        	queryString = String.valueOf(objectMap.get("queryString"));
        }
        
        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String url = "/items/findByCriteria?"+queryString;
        return getResultString(url,entity);
	}

	@Override
	public ResponseEntity<String> findItemSize(Map<String, Object> objectMap) {
		// TODO Auto-generated method stub
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        String queryString = "";
        if(objectMap.get("queryString") != null){
        	queryString = String.valueOf(objectMap.get("queryString"));
        }
        
        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String url = "/items/findItemSize?"+queryString;
        return getResultString(url,entity);
	}

	@Override
	public ResponseEntity<String> save(Map parameter) {
		// TODO Auto-generated method stub
		String url = "/items/save";
		return postWithMapParameter(parameter, HttpMethod.POST, url);
	}

	@Override
	public ResponseEntity<String> dalete(Long id) {
		// TODO Auto-generated method stub
		String url = "/items/"+id;
		return deleteSend(HttpMethod.DELETE, url);
	}

	@Override
	public ResponseEntity<String> getItem(Long id) {
		// TODO Auto-generated method stub
		String url = "/items/"+id;
		return getResultStringByTypeHttpMethodAndBodyContent(HttpMethod.GET,url);
	}

}
