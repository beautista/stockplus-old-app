package com.spt.app.service.impl;

import java.util.Arrays;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.spt.app.service.AbstractEngineService;
import com.spt.app.service.ReportService;

@Service
public class ReportServiceImpl extends AbstractEngineService implements ReportService {

	static final Logger LOGGER = LoggerFactory.getLogger(ReportServiceImpl.class);
	
	@Override
	public ResponseEntity<String> findByCriteriaReportItem(Map<String, Object> objectMap) {
		// TODO Auto-generated method stub
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        String queryString = "";
        if(objectMap.get("queryString") != null){
        	queryString = String.valueOf(objectMap.get("queryString"));
        }
        
        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String url = "/report/item/findByCriteria?"+queryString;

        
        return getResultString(url,entity);
	}

	@Override
	public ResponseEntity<String> findSizeReportItem(Map<String, Object> objectMap) {
		// TODO Auto-generated method stub
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        String queryString = "";
        if(objectMap.get("queryString") != null){
        	queryString = String.valueOf(objectMap.get("queryString"));
        	
        }
        
        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String url = "/report/item/findSize?"+queryString;
        
        return getResultString(url,entity);
	}
	

	@Override
	public ResponseEntity<String> findByCriteriaReportSale(Map<String, Object> objectMap) {
		// TODO Auto-generated method stub
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        String queryString = "";
        if(objectMap.get("queryString") != null){
        	queryString = String.valueOf(objectMap.get("queryString"));
        }
        
        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String url = "/report/sale/findByCriteria?"+queryString;

       
        return getResultString(url,entity);
	}

	@Override
	public ResponseEntity<String> findSizeReportSale(Map<String, Object> objectMap) {
		// TODO Auto-generated method stub
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        String queryString = "";
        if(objectMap.get("queryString") != null){
        	queryString = String.valueOf(objectMap.get("queryString"));
        	
        }
        
        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String url = "/report/sale/findSize?"+queryString;
        
        return getResultString(url,entity);
	}
	
	@Override
	public ResponseEntity<String> findByCriteriaReportSummary(Map<String, Object> objectMap) {
		// TODO Auto-generated method stub
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        String queryString = "";
        if(objectMap.get("queryString") != null){
        	queryString = String.valueOf(objectMap.get("queryString"));
        }
        
        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String url = "/report/summary/findByCriteria?"+queryString;

       
        return getResultString(url,entity);
	}

	@Override
	public ResponseEntity<String> findSizeReportSummary(Map<String, Object> objectMap) {
		// TODO Auto-generated method stub
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");

        String queryString = "";
        if(objectMap.get("queryString") != null){
        	queryString = String.valueOf(objectMap.get("queryString"));
        	
        }
        
        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String url = "/report/summary/findSize?"+queryString;
        
        return getResultString(url,entity);
	}

}
