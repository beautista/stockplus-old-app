package com.spt.app.service.impl;

import java.util.Arrays;
import java.util.Base64;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.deser.std.JsonNodeDeserializer;
import com.spt.app.service.AbstractEngineService;
import com.spt.app.service.UserAuthorizationService;
import com.spt.app.spring.security.CustomUser;
import com.spt.app.spring.security.CustomUserDetailsService;

@Service
public class UserAuthorizationServiceImpl extends AbstractEngineService implements UserAuthorizationService{

	static final Logger LOGGER = LoggerFactory.getLogger(UserAuthorizationServiceImpl.class);
	
	/* For case Special Authentication */
	public Object getUserDetail(String username) {
		// TODO Auto-generated method stub
		HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");
        headers.add("userName",     username);

        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String json = restTemplate.exchange(EngineServer+"/user", HttpMethod.GET, entity, String.class).getBody();
        CustomUser user = gson.fromJson(json, CustomUser.class);
        return user;
	}

}
