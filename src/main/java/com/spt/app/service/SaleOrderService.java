package com.spt.app.service;

import java.util.List;
import java.util.Map;

import org.springframework.http.ResponseEntity;

public interface SaleOrderService {

	public ResponseEntity<String> findByCriteria(Map<String,Object> objectMap);
	public ResponseEntity<String> findSize(Map<String,Object> objectMap);
	public ResponseEntity<String> findSaleOrderItemByCriteria(Map<String,Object> objectMap);
	public ResponseEntity<String> findSaleOrderItemSize(Map<String,Object> objectMap);
	public ResponseEntity<String> findSaleOrderItemCustomerByCriteria(Map<String,Object> objectMap);
	public ResponseEntity<String> findSaleOrderItemCustomerSize(Map<String,Object> objectMap);
}
