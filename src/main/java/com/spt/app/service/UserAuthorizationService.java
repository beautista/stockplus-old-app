package com.spt.app.service;

public interface UserAuthorizationService {

	public Object getUserDetail(String username);
}
