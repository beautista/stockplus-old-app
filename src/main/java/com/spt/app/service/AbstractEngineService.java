package com.spt.app.service;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URI;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.spt.app.spring.security.GrantedAuthorityTypeAdaptor;


public abstract class  AbstractEngineService {
	protected static Logger LOGGER = LoggerFactory.getLogger(AbstractEngineService.class);

	protected String EngineServer = "localhost:8080/Engine/rest";
    protected static Properties connectProperties = null;

    protected String webServicesString = "";
    protected String resultString = "";

    protected RestTemplate restTemplate = new RestTemplate();
    protected JsonParser parser = new JsonParser();

    JsonSerializer<Date> ser = new JsonSerializer<Date>() {
        public JsonElement serialize(Date src, Type typeOfSrc,
                                     JsonSerializationContext context) {
            return src == null ? null : new JsonPrimitive(src.getTime());
        }
    };
    
    JsonDeserializer<Date> deser = new JsonDeserializer<Date>() {
        public Date deserialize(JsonElement json, Type typeOfT,
                                JsonDeserializationContext context) throws JsonParseException {
            return json == null ? null : new Date(json.getAsLong());
        }
    };

    protected Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm").registerTypeAdapter(Date.class, deser).registerTypeAdapter(GrantedAuthority.class, new GrantedAuthorityTypeAdaptor()).create();

    static {
        Resource resource = new ClassPathResource("/server.properties");
        try{
            connectProperties = PropertiesLoaderUtils.loadProperties(resource);

        } catch (IOException e){
            LOGGER.error("Error : {}", e);
        }

    }
    
    public AbstractEngineService(){
        this.EngineServer  = connectProperties.getProperty("EngineServer");
    }
    
    public String getWebServicesString() {
        return webServicesString;
    }
    
    public AbstractEngineService setWebServicesString(String webServicesString) {
        this.webServicesString = webServicesString;
        return this;
    }
    
    public String getResultString() {
        LOGGER.debug("request :{}",getWebServicesString());
        resultString  = restTemplate.getForObject(getWebServicesString(), String.class);
        return resultString;
    }
    
    public String getResultString(String url) {
        LOGGER.debug("request :{}",url);
        resultString  = restTemplate.getForObject(url, String.class);
        return resultString;
    }
    
    public ResponseEntity<String> getResultString(String urlParam,HttpEntity<String> entity) {
    	String url = this.EngineServer +urlParam;
        LOGGER.debug(" request :{}",url);
        return restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
    }
    
    public ResponseEntity<String> getResultStringByTypeHttpMethodAndBodyContent(String json,HttpMethod httpMethod,String url,RestTemplate restTemplate) {
        LOGGER.debug("url :{}", url);
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");



        HttpEntity<String> entity = new HttpEntity<String>(json, headers);
        if(httpMethod==null){
            httpMethod = HttpMethod.GET;
        }
        ResponseEntity<String> reponseEntity = restTemplate.exchange(url, httpMethod, entity, String.class);
        return reponseEntity;
    }
    
    public ResponseEntity<String> getResultStringByTypeHttpMethodAndBodyContent(HttpMethod httpMethod,String urlParam) {
    	String url = this.EngineServer +urlParam;
        LOGGER.info("url :{}", url);
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Content-Type", "application/json; charset=utf-8");



        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        if(httpMethod==null){
            httpMethod = HttpMethod.GET;
        }
        ResponseEntity<String> reponseEntity = restTemplate.exchange(url, httpMethod, entity, String.class);
        return reponseEntity;
    }
    
    public ResponseEntity<String> postWithMapParameter(Map<String, String[]> parameterMap,HttpMethod httpMethod,String urlParam) {
    	String url = this.EngineServer +urlParam;
    	LOGGER.info("url :{}", url);

        MediaType mediaType = new MediaType("application","x-www-form-urlencoded", Charset.forName("UTF-8"));
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(mediaType);

        MultiValueMap<String, String> body = new LinkedMultiValueMap<String, String>();  
        if(parameterMap!=null){
        	for(String key:parameterMap.keySet()){
        		if(!"parentId".equals(key) && !"_csrf".equals(key)){
        			if(  key.toUpperCase().endsWith("DATE") ){
                        try {
							body.add(key, new Timestamp(Long.parseLong(parameterMap.get(key)[0])).toString());
						} catch (NumberFormatException e) {  }
        			}else{
        				body.add(key, parameterMap.get(key)[0]);
        			}
        			
        		}
        		
        		
        	}
        }


        HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<MultiValueMap<String, String>>(body, headers);
        if(httpMethod==null){
            httpMethod = HttpMethod.POST;
        }
        
        FormHttpMessageConverter converter = new FormHttpMessageConverter();
        converter.setSupportedMediaTypes(Arrays.asList(mediaType));
        
        restTemplate.getMessageConverters().add(converter);
        ResponseEntity<String> reponseEntity = restTemplate.postForEntity(url, entity, String.class);
        return reponseEntity;
    }
    
    public ResponseEntity<String> deleteSend(HttpMethod httpMethod,String urlParam) {
    	String url = this.EngineServer +urlParam;
    	LOGGER.info("url :{}", url);

        HttpHeaders headers = new HttpHeaders();

        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        
        restTemplate.delete(url);
        return new ResponseEntity("", HttpStatus.OK);
    }
    
    
}
