package com.spt.app.service;

import java.util.List;
import java.util.Map;

import org.springframework.http.ResponseEntity;

public interface PosService {
	public ResponseEntity<String> findByCriteria(Map<String,Object> objectMap);
	public ResponseEntity<String> findSize(Map<String,Object> objectMap);
	public ResponseEntity<String> deleteSalesTransactionByUser(String saler);
	public ResponseEntity<String> deleteSalesTransaction(Long id);
	public ResponseEntity<String> saveSalesTransaction(Map parameter);
	public ResponseEntity<String> saveSo(Map parameter);
	public ResponseEntity<String> printSlip(String soNumber);
}
