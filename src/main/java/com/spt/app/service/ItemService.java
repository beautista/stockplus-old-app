package com.spt.app.service;

import java.util.List;
import java.util.Map;

import org.springframework.http.ResponseEntity;

public interface ItemService {
	
	public ResponseEntity<String> findByCriteria(Map<String,Object> objectMap);
	public ResponseEntity<String> findItemSize(Map<String,Object> objectMap);
	public ResponseEntity<String> save(Map parameter);
	public ResponseEntity<String> dalete(Long id);
	public ResponseEntity<String> getItem(Long id);
}
