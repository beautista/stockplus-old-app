Application Feature
1.Jave 8,Spring 4 ,Tomcat 8.5 base only
2.Spring Security Support(iMemory,ldap,webservice)
3.Support Serve Static Resources 
4.Support Resource Versioning(css,js)

    Unversioning : <spring:url value="/resources/scripts/sockjs-0.3.4.js" var="sockjs_js" />
    Versioning : <spring:url value="${urls.getForLookupPath('/resources/scripts/stomp.js')}"        var="stomp_js" />
        
	Useage : 
	         <spring:url value="${urls.getForLookupPath('/resources/scripts/test.js')}" var="testxx" />
             <script type="text/javascript"  src="${testxx}">
5.Support Detect for Tomcat Attribute(Use in Counter web and Static use)
6.Member last access link url(click link from url)
7.Have UserModel is an object By SESSION SCOPE
8.Interceptor remember search into cookie
              findByCriteria
              listView
              ${criteria_name}
9.Integrate Web Socket(Demo Chat application)
10.Support I18N
11.Support Upload File(MultipartResolver)
12.Support Apache Tiles 3
13.Logging Framework use SLF4J+lockback
	- user & session & warfile
	- view log online as  /resources/logs/index.html full path http://localhost:8080/XDocApp/resources/logs/index.html
	- append log to catalina log By AppName and rolling zip file
	- append log to Console
	- view log online real time as /viewlog full path http://localhost:8080/XDocApp/viewlog

14.Storeage Parameter in IndexedDb(local storerage)
    Ex.
        function getPlant() {
	     $PARAMETER_FETCH.store =  $PARAMETER_FETCH.db.transaction(["parameter"]).objectStore("parameter")
	     var request = $PARAMETER_FETCH.store.get('P100');
	     
	     request.onerror = function(event) {
	     };
	     
	     request.onsuccess = function(event) {
	        // Do something with the request.result!
	        if(request.result) {
	           alert("description: " + request.result.parameterDetails );
	        }
	        else {
	           alert("No Parameter Setup!");
	           console.log("No Parameter in local memory chould be call ajax request");
	        }
	     };
	  }


[Test]
-Master Data 
	- Flag Active
	- Sequence
	- code
	- description
	- externalCode
- IOS icon Support
+Default HTML Page /login /logout /accessDenied /exception
- token for access one time
- Special Charector Auto Convert
- stop server session is expire(Test On Server)
- JMS (what benefit) 
- JMX Fuature(what benefit)

